# О проекте
[Профилировщик PHP](https://github.com/badoo/liveprof)


## Первоначальное разворачивание проекта
```bash
cp src/config/services.yaml.dist src/config/services.yaml
# Внести изменения в src/config/services.yaml в соответствии с окружением

./install.sh
```
