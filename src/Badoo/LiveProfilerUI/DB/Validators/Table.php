<?php declare(strict_types=1);

/**
 * @maintainer Timur Shagiakhmetov <timur.shagiakhmetov@corp.badoo.com>
 */

namespace Badoo\LiveProfilerUI\DB\Validators;

use Badoo\LiveProfilerUI\DataProviders\Job;
use Badoo\LiveProfilerUI\DataProviders\Method;
use Badoo\LiveProfilerUI\DataProviders\MethodData;
use Badoo\LiveProfilerUI\DataProviders\MethodTree;
use Badoo\LiveProfilerUI\DataProviders\Snapshot;
use Badoo\LiveProfilerUI\DataProviders\Source;
use Badoo\LiveProfilerUI\Exceptions\InvalidTableNameException;

class Table
{
    /** @var string[] */
    protected static $allowed_table_names = [
        Source::TABLE_NAME,
        Snapshot::TABLE_NAME,
        MethodTree::TABLE_NAME,
        MethodData::TABLE_NAME,
        Method::TABLE_NAME,
        Job::TABLE_NAME,
    ];

    public static function validate(string $table) : bool
    {
        if (!\in_array($table, self::$allowed_table_names, true)) {
            throw new InvalidTableNameException('Invalid table name: ' . $table);
        }

        return true;
    }
}
