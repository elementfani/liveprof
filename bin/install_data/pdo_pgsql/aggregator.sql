CREATE TABLE IF NOT EXISTS liveprof.aggregator_methods (
  id SERIAL NOT NULL PRIMARY KEY,
  name CHAR(300) NOT NULL,
  date date NOT NULL DEFAULT '1970-01-01'
);
CREATE UNIQUE INDEX IF NOT EXISTS name_idx on liveprof.aggregator_methods (name);

DO $$
BEGIN
IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'types') THEN
create type types AS ENUM('auto','manual');
END IF;
END
$$;

CREATE TABLE IF NOT EXISTS liveprof.aggregator_snapshots (
  id SERIAL NOT NULL PRIMARY KEY,
  calls_count INT NOT NULL,
  app CHAR(32) DEFAULT NULL,
  date date NOT NULL,
  label CHAR(100) DEFAULT NULL,
  type types NOT NULL DEFAULT 'auto',
  %SNAPSHOT_CUSTOM_FIELDS%
);
CREATE INDEX IF NOT EXISTS app_idx ON liveprof.aggregator_snapshots (app);

CREATE TABLE IF NOT EXISTS liveprof.aggregator_tree (
  id SERIAL NOT NULL PRIMARY KEY,
  snapshot_id INT NOT NULL,
  method_id INT references liveprof.aggregator_methods(id),
  parent_id INT references liveprof.aggregator_methods(id),
  %TREE_CUSTOM_FIELDS%
);
CREATE INDEX IF NOT EXISTS snapshot_id_parent_id_idx ON liveprof.aggregator_tree (snapshot_id, parent_id);
CREATE INDEX IF NOT EXISTS snapshot_id_method_id_idx ON liveprof.aggregator_tree (snapshot_id, method_id);

CREATE TABLE IF NOT EXISTS liveprof.aggregator_method_data (
  id SERIAL NOT NULL PRIMARY KEY,
  snapshot_id INT references liveprof.aggregator_snapshots(id),
  method_id INT references liveprof.aggregator_methods(id),
  %DATA_CUSTOM_FIELDS%
);
CREATE INDEX IF NOT EXISTS aggregator_method_data_ibfk_1 ON liveprof.aggregator_method_data (snapshot_id,method_id);
CREATE INDEX IF NOT EXISTS aggregator_method_data_ibfk_2 ON liveprof.aggregator_method_data (method_id);
